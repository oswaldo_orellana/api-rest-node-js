import { Router } from "express";
import { getAnuncios } from "../app/controllers/anuncios.controller";
import { getCategorias, getCategoriaById, createNewCategoria, deleteCategoriaById, updateById } from "../app/controllers/categoria.controller";
import { deleteUserAnuncio, getUserAnuncios } from "../app/controllers/useranuncios.controller";
import { getAllUserFavoritos, deleteUserFavorito, addUserAFavorito } from "../app/controllers/userfavorito.controller";
import { getUsers, getUsersById, createNewUser } from "../app/controllers/users.controller";


const router = Router();


//CRUD  Categoria
router.post("/categorias", createNewCategoria);
router.get("/categorias", getCategorias);
router.get("/categorias/:categoriasId", getCategoriaById);
router.put("/categorias/:categoriasId", updateById);
router.delete("/categorias/:categoriasId", deleteCategoriaById);
//CRUD Categoria end


//CRUD USER BEGIN 
router.get("/users/", getUsers);
router.get("/users/:userId", getUsersById);
router.post("/users", createNewUser);
//CRUD USER END

//CRUS USER FAVORITOS
router.get("/users/:user_id/favoritos/", getAllUserFavoritos);
router.delete("/users/:user_id/favoritos/:anuncio_id/", deleteUserFavorito);
router.post("/users/:user_id/favoritos/", addUserAFavorito);
//CRUD USER FAVORITOS

// GET user anuncios
router.get("/misanuncios/users/:user_id", getUserAnuncios);

//Delete anuncios
router.delete("/anuncios/:anuncio_id", deleteUserAnuncio);
router.get("/anuncios/", getAnuncios);


export default router;
