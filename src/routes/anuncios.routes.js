import { Router } from "express";

import { createNewAnuncio, getAnuncios } from "../app/controllers/anuncios.controller";

const router = Router()

router.get("/anuncios", getAnuncios);
router.get("/anuncios", () => { });

router.post("/anuncios", createNewAnuncio);

router.delete("/anuncios", getAnuncios);

router.put("/anuncios", getAnuncios);



export default router;