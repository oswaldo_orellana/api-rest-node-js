import Anuncio from "../models/anuncio.model";
import Foto from "../models/foto.model"
import { showAll, errorResponse } from './response/response';

export const getAnuncios = async (req, res) => {
    try {
        const anuncios = await Anuncio.findAll({
            order: [
                ['fecha_publicacion', 'DESC']
            ],
            include: [{
                attributes: ["id", "enlace", "anuncio_id"],
                model: Foto,
            }]
        });
        showAll(res, anuncios);
    } catch (error) {
        errorResponse(res, error, 500);
    }
}

export const getAnunciosDestacados = async (req, res) => {

}