import User from "../models/user.model";
import Anuncio from "../models/anuncio.model";
import { showOne, showAll, errorResponse, deleteResponse } from "./response/response";


export const getUserAnuncios = async (req, res) => {
    var id = req.params.user_id;
    try {
        const anuncios = await User.findByPk(id, {
            include: [{
                model: Anuncio,
            }]
        });
        if (anuncios == null) return errorResponse(res, `id ${id} no encontrado`, 400);
        return showAll(res, anuncios.anuncios);
    } catch (error) {
        errorResponse(res, error, 500);
    }
}


export const deleteUserAnuncio = async (req, res) => {
    var user_id = req.params.user_id;
    var anuncio_id = req.params.anuncio_id;
    try {
        const anuncio = await Anuncio.findByPk(anuncio_id);
        await anuncio.destroy();
        deleteResponse(res, "eliminado con exito", 204);
    } catch (error) {
        console.log(error);
    }
}