import User from "../models/user.model";
import Anuncio from "../models/anuncio.model";
import Foto from "../models/foto.model";
import { showAll, errorResponse, deleteResponse, showOne } from '../controllers/response/response';
import Favorito from "../models/favorito.model";


export const getAllUserFavoritos = async (req, res) => {
    console.log(req.params.user_id);
    const user_id = req.params.user_id;
    try {
        const favoritos = await User.findOne({
            where: {
                id: user_id
            },
            include: [{
                model: Anuncio,
                exclude: ["favoritos"],
                through: { attributes: [] },
                include: [{
                    model: Foto,
                    attributes: {
                        exclude: ["createdAt", "updatedAt", "anuncioId"],
                    },
                }],
                attributes: {
                    exclude: ["created_at", "updated_at", "createdAt", "updatedAt", "favoritos"],
                },
            }],
        }
        );
        if (favoritos == null) return errorResponse(res, `el usuario ${user_id} no existe`, 400);
        return showAll(res, favoritos.anuncios);
    } catch (error) {
        errorResponse(res);
    }
}

export const deleteUserFavorito = async (req, res) => {
    console.log(req.params);
    const { user_id, anuncio_id } = req.params;
    try {
        const favorito = await Favorito.findOne({
            where: {
                user_id: user_id,
                anuncio_id: anuncio_id,
            }
        });
        if (favorito == null) return errorResponse(res, `no se encontro el user_id ${user_id} o el anuncio_id ${anuncio_id}`, 400);
        await favorito.destroy();
        return deleteResponse(res, "eliminado", favorito);
    } catch (error) {
        errorResponse(res);
    }
}

export const addUserAFavorito = async (req, res) => {
    const { user_id } = req.params;
    const { anuncio_id } = req.body;
    try {
        const anuncio = await Anuncio.findByPk(anuncio_id);
        const user = await User.findByPk(user_id);
        if (user == null || anuncio == null) return errorResponse(res, `No se encontro el user ${user_id} o el anuncio ${anuncio_id}`);
        var result = await Favorito.findOne({
            where: {
                user_id: user_id,
                anuncio_id: anuncio_id,
            },
        });
        if (result == null) {
            result = await anuncio.addUser(user);
            return showOne(res, result, 201);
        }
        errorResponse(res, "ya esta en favoritos", 204);
    } catch (error) {
        console.log(error);
        if (error.errno == 1062) { //duplicated favorito
            res.status(400);
            res.send({ "code": 400, "msg": " este anuncios ya esta en tu favoritos" });
            return;
        }
        res.status(500).send({ "code": 500, "msg": "hubo un problema al solicitar su peticion intente de nuevo" });
    }

}
