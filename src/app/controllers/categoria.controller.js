import Categoria from "../models/categoria.model";
import { errorResponse, showAll, showOne, deleteResponse } from "./response/response";
import Credito from "../models/credito.model";

export const getCategorias = async (req, res) => {
    try {
        const categorias = await Categoria.findAll();
        showAll(res, categorias);
    } catch (error) {
        error(res);
    }
}

export const getCategoriaById = async (req, res) => {
    var id = req.params.categoriasId;
    try {
        const categoria = await Categoria.findByPk(id);
        return categoria != null ? showOne(res, categoria) : errorResponse(res, "id no encontrado", 400);
    } catch (error) {
        error(res);
    }
}

function error(res) {
    errorResponse(res, "error al processed los datos", 500);
}

export const createNewCategoria = async (req, res) => {
    const { nombre, imagen } = req.body;
    let { categoria_id } = req.body;
    if (!verify(nombre, imagen)) {
        return errorResponse(res, "parametros incorrectos", 400);
    }
    try {
        const categoria = new Categoria({ nombre, imagen, categoria_id });
        await categoria.save();
        showOne(res, categoria, 201);
    } catch (error) {
        console.log(error);
        error(res);
    }
}

function verify(nombre, imagen) {
    return nombre != "" && imagen != "";
}

export const deleteCategoriaById = async (req, res) => {
    var id = req.params.categoriasId;
    try {
        const categoria = await Categoria.findByPk(id, {
            include: [{
                model: Credito,
            }]
        });
        if (categoria != null) {
            if (categoria.credito_destacado == null) {
                await categoria.destroy();
                return deleteResponse(res, "eliminado con éxito", categoria, 204);
            } else {
                return errorResponse(res, "categoria tiene un credito", 500);
            }
        } else {
            return errorResponse(res, `no se encontró el ${id} categoria`, 400);
        }
    } catch (error) {
        res.status(500).send(error.message);
    }
}



export const updateById = async (req, res) => {
    var id = req.params.categoriasId;
    const { nombre, imagen } = req.body;
    let { categoria_id } = req.body;

    try {
        const categoria = await Categoria.findByPk(id);
        if (categoria == null) {
            return errorResponse(res, `No sé encontro el id ${id} especificado`, 400);
        }
        categoria.nombre = validate(nombre) ? categoria.nombre : nombre;
        categoria.imagen = validate(imagen) ? categoria.imagen : imagen;
        categoria.categoria_id = categoria_id;
        await categoria.save();
        return showOne(res, categoria, 201);
    } catch (error) {
        console.log(error);
        error(res);
    }
}
function validate(any) {
    return any == "" || any == null;
}