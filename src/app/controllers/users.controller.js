import { getConnection } from "../database/connection";
import { usersQuery } from "../models/querys_models/users.querys";
import User from "../models/user.model";
import { showAll, errorResponse, showOne } from "./response/response"

function errorResponse1(res) {
    errorResponse(res, "ocurio un error", 500);
}


export const getUsers = async (req, res) => {
    try {
        const users = await User.findAll();
        return showAll(res, users);
    } catch (error) {
        errorResponse1(res);
    }
}

export const getUsersById = async (req, res) => {
    var id = req.params.userId;
    try {
        const user = await User.findByPk(id);
        if (user == null) return errorResponse(res, `no se existe el user con el id: ${id}`, 400);
        return showOne(res, user);
    } catch (error) {
        errorResponse(error, res);
    }
}

function validateUserAndPassword(name, email, password) {
    return name != null && email != null && password != null || name != "" && email != "" && password != "";
}

export const createNewUser = async (req, res) => {

    const { nombre, email } = req.body;
    let { password } = req.body;
    if (!validateUserAndPassword(nombre, email, password)) {
        return errorResponse(res, `los campos no deben estar vacios`, 400);
    }

    try {
        const user = await User.findOrCreate({
            where: { email: email },
            defaults: { name: nombre, email: email, password: password, },
        });
        if (user[1]) return showOne(res, "creado con exito");
        return errorResponse(res, `el correo ya esta registrado`, 400);
    } catch (error) {
        console.log(error);
    }

    /*try {

        const [addUser, fields] = await pool.execute(usersQuery.addNewUser, [name, email, password]);
        console.log(addUser);
        res.json({ "code": 200, "data": addUser.affectedRows });
    } catch (error) {
        errorResponse(error, res);
    }*/
}

