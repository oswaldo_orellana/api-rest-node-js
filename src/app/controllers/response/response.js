export const showAll = (res, data, code = 200) => {
    return res.status(code).send({ "code": code, "data": data });
}

//mostrar uno o mostrar si se ha agregado
export const showOne = (res, data, code = 200) => {
    return res.status(code).send({ "code": code, "data": data });
}


export const errorResponse = (res, msg = "error en el servidor ", code = 500) => {
    return res.status(code).send({ "code": code, "msg": msg });
}

export const deleteResponse = (res, msg, data, code = 204) => {
    return res.status(code).send({ "code": code, "msg": msg, "data": data });
}
