import { DataTypes } from "sequelize";
import dbSequelize from "../database/connection";

const Categoria = dbSequelize.define('categorias', {
    nombre: {
        type: DataTypes.STRING,
    },
    imagen: {
        type: DataTypes.STRING,
    },
});

export default Categoria;
