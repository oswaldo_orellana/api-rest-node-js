import { DataTypes } from "sequelize";
import dbSequelize from "../database/connection";

const User = dbSequelize.define('users', {
    name: {
        type: DataTypes.STRING,
    },
    email: {
        type: DataTypes.STRING,
    },
    email_verified_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    }
});

export default User;