import { DataTypes, Model } from "sequelize";
import dbSequelize from "../database/connection";

const Anuncio = dbSequelize.define('anuncios', {
    titulo: {
        type: DataTypes.STRING,
    },
    precio: {
        type: DataTypes.DOUBLE,
        allowNull: true,
    },
    fecha_publicacion: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    condicion_encuentra: {
        type: DataTypes.STRING,
    },
    ubicacion: {
        type: DataTypes.STRING,
    },
    enlace: {
        type: DataTypes.STRING,
        validate: {
            isUrl: true,
        }
    },
    descripcion: {
        type: DataTypes.STRING,
    },
});

export default Anuncio;