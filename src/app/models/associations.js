import Anuncio from "./anuncio.model";
import Categoria from "./categoria.model";
import Credito from "./credito.model";
import Foto from "./foto.model";
import User from "./user.model";
import Favorito from "./favorito.model";

//categoria con credito begin
Categoria.hasOne(Credito);
Credito.belongsTo(Categoria);
//categoria con credito end




//user con anuncio
User.hasMany(Anuncio);
Anuncio.belongsTo(User);
//

//anuncio con fotos
Anuncio.hasMany(Foto);
Foto.belongsTo(Anuncio);

//favoritos

User.belongsToMany(Anuncio, { through: Favorito });
Anuncio.belongsToMany(User, { through: Favorito });

