import { DataTypes } from "sequelize";
import dbSequelize from "../database/connection";

const Credito = dbSequelize.define("credito_destacados", {
    cantidad: {
        type: DataTypes.INTEGER,
    },
    categoria_id: {
        type: DataTypes.BIGINT,
    }
});

export default Credito;