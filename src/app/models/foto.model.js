import { DataTypes } from "sequelize";
import dbSequelize from "../database/connection";

const Foto = dbSequelize.define("fotos", {
    enlace: {
        type: DataTypes.STRING,
        validate: {
            isUrl: true,
        },
    },
    anuncio_id: {
        type: DataTypes.BIGINT,
    }
});

export default Foto;