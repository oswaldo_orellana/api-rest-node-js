export const usersQuery = {
    getUsers: "SELECT * FROM users",
    getUsersById: "SELECT * FROM users WHERE id = ?",
    addNewUser: "INSERT INTO users (name,email,password) VALUES (?,?,?)",
    getUserByEmail: "SELECT * FROM users WHERE email = ?",
};