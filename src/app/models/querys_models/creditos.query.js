export const creditosQuery = {
    //getAllCategorias: "SELECT * FROM Categorias",
    getCreditoById: "SELECT * FROM credito_destacados WHERE id= ?",
    //addNewCategoria: "INSERT INTO categorias (nombre,imagen,categoria_id) VALUES (?,?,?)",
    deleteCredito: "DELETE FROM creditos WHERE id = ?",
    //updateCategoria: "UPDATE categoria SET nombre = @nombre, imagen= @imagen,categoria_id=@categoria_id",
    getCreditoByCategoriaId: "SELECT * FROM credito_destacados WHERE categoria_id = ? ",
    deleteCreditoByCategoriaId: "DELETE FROM credito_destacados WHERE categoria_id = ?",
}