export const userFavoritos = {
    getUserFavoritos: "SELECT * FROM anuncios a, favoritos f WHERE a.id = f.anuncio_id and f.user_id = ?",
    deleteUserFavoritosByUserAnuncio: "DELETE FROM favoritos WHERE user_id = ? and anuncio_id = ? ",
    addNewFavorito: "INSERT INTO favoritos (user_id,anuncio_id) values (?,?)",
}

