export const categoriaQuery = {
    getAllCategorias: "SELECT * FROM Categorias",
    getCategoriaById: "SELECT * FROM categorias WHERE id= ?",
    addNewCategoria: "INSERT INTO categorias (nombre,imagen,categoria_id) VALUES (?,?,?)",
    deleteCategoria: "DELETE FROM categorias WHERE id = ?",
    updateCategoriaById: "UPDATE categorias SET nombre = ?, imagen = ?,categoria_id = ? WHERE id = ? ",
}