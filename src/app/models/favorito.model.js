import { DataTypes } from "sequelize";
import dbSequelize from "../database/connection";


const Favorito = dbSequelize.define('favoritos', {

    user_id: {
        type: DataTypes.BIGINT,
    },
    anuncio_id: {
        type: DataTypes.BIGINT,
    },
}, {

});

export default Favorito;