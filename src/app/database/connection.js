import mysql2 from 'mysql2/promise';
import { Sequelize } from 'sequelize';
import config from "../config";


var cfg = {
    host: config.dbServerHost,
    user: config.dbUser,
    password: config.dbPassword,
    database: config.dbDatabaseName,
    port: config.dbPort,
};

export const getConnection = async () => {
    try {
        const pool = await mysql2.createConnection(cfg);
        return pool;
    } catch (error) {
        console.error(error);
    }
}




const dbSequelize = new Sequelize(config.dbDatabaseName, config.dbUser, config.dbPassword, {
    host: config.dbServerHost,
    dialect: 'mariadb',
    define: {
        underscored: true,
    }

});

export const getConnectionSequelize = async () => {
    try {
        await dbSequelize.authenticate();
        console.log("success");
    } catch (error) {
        console.log("error: " + error);
    }
}

export { mysql2 };

export default dbSequelize;