import { config } from 'dotenv';
config();

export default {
    port: process.env.PORT || 3000,
    dbUser: process.env.DB_USER || '',
    dbPassword: process.env.DB_PASSWORD || '',
    dbServerHost: process.env.DB_SERVER || "",
    dbDatabaseName: process.env.DB_DATABASE_NAME || "",
    dbPort: process.env.DB_PORT,
}