import express from 'express'
import config from './config';
import cors from "cors";
import morgan from 'morgan';

import basicRouter from '../routes';
import { getConnectionSequelize } from './database/connection';
import "./models/associations";


const app = express();
//  Setting
app.set("port", config.port);

// Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

getConnectionSequelize();


app.use('/', basicRouter);

export default app;