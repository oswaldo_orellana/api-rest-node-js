IMPORTANTE
/*****
1.- TENER INSTALADO nodejs
2.- EJECUTAR EL COMANDO npm install PARA INSTALAR LAS DEPENDENCIAS/MODULO del proyecto
/******

Script base datos

1.- INSTALAR XAMPP u otro servidor con mariadb mysql
2.- ENTRAR AL ADMIN DE LA BASE DE DATOS Y CREAR UNA BASE DE DATOS con el NOMBRE anuncios u otro nombre
3.- seleccionar la base de datos y dar click en la opcion IMPORTAR
4.- DAR CLICK EN SELECCIONAR A ARCHIVO e ir a donde esta el ARCHIVO "scripts database/anuncio.sql" CLICK EN SELECCIONAR Y CLICK EN CONTINUAR 
5.- y listo le deben salir las tablas con algunos registros faker

CONECTANDO EL PROYECTO CON LA BASE DE DATOS Y ALISTANDO PARA SU EJECUCION

1.- CREAR UN ARCHIVO con el NOMBRE .env en la carpeta anuncio-api
2.- CREAREMOS variables q vamos a necesitar 
    --------------------------------------------------------
    PORT => puerto que va ocupar el node js
    DB_SERVER_HOST => host de la base de datos
    DB_DATABASE_NAME => nombre de la base de datos 
    DB_USER => el usuario de tu base de datos
    DB_PASSWORD => el password de tu base de datos
    DB_PORT => el puerto que esta ocupando tu base de datos
    -------------------------------------------------------
3.- EJEMPLO de como deberia quedar

    PORT = 3000
    DB_SERVER_HOST = localhost
    DB_DATABASE_NAME = anuncios
    DB_USER = root
    DB_PASSWORD = 
    DB_PORT = 3306

4.- listo ya tenemos la configuracion.

EJECUTAR npm run dev PARA HACER LAS PRUEBAS

SOLO VISUAL STUDIO CODE 
PARA EJECUTAR LOS ARCHIVO DE LA CARPETA request TENER INSTALADO la extension REST Client 


 